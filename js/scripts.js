jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>10){
			$('html').addClass('on');
		} else {
			$('html').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('.round,.mouse').on('click',function(event){
		event.preventDefault();
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});

});